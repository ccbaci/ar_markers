QT += core gui widgets opengl
TARGET = ar_markers
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp \
    form.cpp \
    process.cpp \
    capture.cpp \
    widget.cpp

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += opencv libv4l2

FORMS += \
    form.ui

HEADERS += \
    form.h \
    buffer.h \
    global.h \
    process.h \
    capture.h \
    structures.h \
    widget.h
