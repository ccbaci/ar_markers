#ifndef PROCESS_H
#define PROCESS_H

#include "global.h"
#include "buffer.h"
#include "structures.h"

class Process : public QThread
{
public:
    Process() = delete;
    Process(Buffer<PFrame> & buffer_in, Buffer<PFrame> & buffer_out);

    void end() { m_isEnd = true; }

protected:
    void run();

private:
    cv::Ptr<cv::aruco::Dictionary> m_dict;
    cv::Ptr<cv::aruco::DetectorParameters> m_params;

    QMap<int, QVector<QPointF>> m_markerCorners;

    Buffer<PFrame> * m_buffer_in;
    Buffer<PFrame> * m_buffer_out;

    bool m_isEnd;

};

#endif // PROCESS_H
