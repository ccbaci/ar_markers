#include "form.h"
#include "ui_form.h"

Form::Form(QWidget * parent) : QWidget(parent), ui(new Ui::Form)
{
    ui->setupUi(this);

    // buffers
    buffer_cap.setCapacity(10);
    buffer_cap.setDrop(true);
    buffer_pro.setCapacity(10);
    buffer_pro.setDrop(true);

    // paint timer
    timer = new QTimer();
    thread = new QThread();
    timer->setInterval(40);
    timer->moveToThread(thread);
    connect(thread, SIGNAL(started()), timer, SLOT(start()));
    connect(thread, SIGNAL(finished()), timer, SLOT(stop()));
    connect(timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    thread->start();

    // process
    process = new Process(buffer_cap, buffer_pro);
    process->start();

    // capture
    capture = new Capture(buffer_cap);
    capture->setMode(Capture::File);
    capture->start();
}

Form::~Form()
{
    capture->end();
    process->end();
    thread->quit();

    capture->wait();
    process->wait();
    thread->wait();

    delete ui;
}

void Form::onTimeout()
{
    frame = buffer_pro.take();
    if(!frame.isValid || !frame.isCalculated)
        return;

    ui->widget->setFrame(frame);
}
