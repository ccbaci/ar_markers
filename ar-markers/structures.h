#ifndef STRUCTURES_H
#define STRUCTURES_H

#include "global.h"

struct PFrame {
    bool isValid;
    bool isCalculated;

    cv::Mat image_cv;
    QImage image;

    std::map<int, std::vector<cv::Point2f>> corners_cv;
    int marker_id;

    cv::Mat homography_cv;
    QTransform homography;
};

#endif // STRUCTURES_H
