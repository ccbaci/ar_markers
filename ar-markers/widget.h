#ifndef WIDGET_H
#define WIDGET_H

#include "global.h"
#include "structures.h"

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget * parent = nullptr);

    void setFrame(PFrame & frame);

    void paintEvent(QPaintEvent * event);

private:
    PFrame * m_frame;

};

#endif // WIDGET_H
