#include "capture.h"

Capture::Capture(Buffer<PFrame> &buffer) :
    m_buffer(&buffer),
    m_isEnd(0)
{

}

void Capture::run()
{
    try {
        // stop exposure control
        if(m_mode = Camera) {
            int d = v4l2_open("/dev/video0", O_RDWR);
            v4l2_control c;
            c.id = V4L2_CID_EXPOSURE_AUTO_PRIORITY;
            c.value = 0;
            v4l2_ioctl(d, VIDIOC_S_CTRL, &c);
            v4l2_close(d);
        }

        // open capture
        m_capture.reset(new cv::VideoCapture(CAPTURE_LOC));
        if(!m_capture->isOpened()) {
            throw -1;
        }

        // get props
        m_fps = m_capture->get(cv::CAP_PROP_FPS);
        m_resolution.setWidth(m_capture->get(cv::CAP_PROP_FRAME_WIDTH));
        m_resolution.setHeight(m_capture->get(cv::CAP_PROP_FRAME_HEIGHT));
        if(m_fps == 0) {
            throw -2;
        }

        m_runtime = 1000. / m_fps;
    }
    catch(int & error) {
        switch(error) {
        case -1:
            qDebug() << "ERROR" << "camera cannot be opened";
            break;
        case -2:
            qDebug() << "ERROR" << "frame rate is not accessible";
            break;
        }
    }

    // thread exec push next frame to buffer
    m_isEnd = false;
    while(!m_isEnd) {
        m_time.restart();

        PFrame frame;
        m_capture->read(frame.image_cv);
        if(frame.image_cv.empty())
            break;

        frame.image = QImage(frame.image_cv.data, frame.image_cv.cols, frame.image_cv.rows, frame.image_cv.step, QImage::Format_RGB888);
        frame.isValid = true;
        frame.isCalculated = false;
        m_buffer->add(frame);

        double elapsed = m_time.elapsed();
        if(elapsed + 2 < m_runtime) {
            thread()->msleep(m_runtime - elapsed);
        }
    }
}
