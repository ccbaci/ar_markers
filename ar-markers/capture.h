#ifndef CAPTURE_H
#define CAPTURE_H

#include "global.h"
#include "buffer.h"
#include "structures.h"
#define CAPTURE_LOC std::string("/home/canberk/Desktop/ar_markers_test/test_input/test5.mp4")

class Capture : public QThread
{
    Q_OBJECT

public:
    enum CaptureMode {
        Camera,
        File
    };
    Capture() = delete;
    Capture(Buffer<PFrame> & buffer);

    void setMode(const CaptureMode & mode) { m_mode = mode; }
    void end() { m_isEnd = true; }

protected:
    void run();

private:
    QScopedPointer<cv::VideoCapture> m_capture;

    CaptureMode m_mode;
    double m_fps;
    double m_runtime;
    QSize m_resolution;

    QTime m_time;

    Buffer<PFrame> * m_buffer;
    bool m_isEnd;

};

#endif // CAPTURE_H
