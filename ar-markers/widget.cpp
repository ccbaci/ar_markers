#include "widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    m_frame = nullptr;
}

void Widget::setFrame(PFrame & frame)
{
    m_frame = &frame;
    update();
}

void Widget::paintEvent(QPaintEvent * event)
{
    QPainter painter(this);

    if(!m_frame) return;

    // draw frame
    painter.drawImage(rect(), m_frame->image);

    // draw markers
    painter.setPen(Qt::red);
    painter.setBrush(Qt::NoBrush);
    std::vector<cv::Point2f> corners_cv = m_frame->corners_cv[m_frame->marker_id];
    QVector<QPointF> corners;
    for(int i = 0; i < corners_cv.size(); ++i)
        corners.append(QPointF(corners_cv[i].x, corners_cv[i].y));

    painter.drawPolygon(corners);
    painter.setPen(QPen(QBrush(Qt::yellow), 4));
    painter.drawPoint(corners[0]);
    painter.setPen(QPen(QBrush(Qt::blue), 4));
    painter.drawPoint(corners[1]);
    painter.setPen(QPen(QBrush(Qt::green), 4));
    painter.drawPoint(corners[2]);
    painter.setPen(QPen(QBrush(Qt::cyan), 4));
    painter.drawPoint(corners[3]);
    painter.setPen(QPen(QBrush(QColor(255, 255, 0, 127)), 2));

    QVector<QLineF> lines;
    for(int i = 0; i <= 500; i += 50) {
        lines.append(m_frame->homography.map(QLineF(QPointF(0, i), QPointF(450, i))));
    }
    for(int j = 0; j <= 450; j += 50) {
        lines.append(m_frame->homography.map(QLineF(QPointF(j, 0), QPointF(j, 500))));
    }
    painter.drawLines(lines);
}
