#ifndef FORM_H
#define FORM_H

#include "global.h"
#include "capture.h"
#include "process.h"
#include "buffer.h"
#include "widget.h"
#include "structures.h"

namespace Ui { class Form; }

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget * parent = nullptr);
    ~Form();

    Capture * capture;
    Process * process;
    Buffer<PFrame> buffer_cap, buffer_pro;

    QTimer * timer;
    QThread * thread;

    PFrame frame;

public slots:
    void onTimeout();

private:
    Ui::Form * ui;
};

#endif // FORM_H
