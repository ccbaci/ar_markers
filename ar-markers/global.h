#ifndef GLOBAL_H
#define GLOBAL_H

#include <QImage>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QPainter>
#include <QWidget>
#include <QObject>
#include <QThread>
#include <QTime>
#include <QTimer>
#include <QVector>
#include <QMap>
#include <QGLWidget>
#include <QPaintEvent>

#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/video/tracking.hpp>

#include <libv4l2.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#endif // GLOBAL_H
