#include "process.h"

Process::Process(Buffer<PFrame> & buffer_in, Buffer<PFrame> & buffer_out) :
    m_buffer_in(&buffer_in),
    m_buffer_out(&buffer_out),
    m_isEnd(0)
{
    // prepare dictionary
    m_dict = new cv::aruco::Dictionary();

    m_dict->markerSize = 6;
    m_dict->maxCorrectionBits = 2;

    uchar marker_O[36] = { 0,0,1,1,1,1, 0,1,0,0,1,1, 1,0,0,0,0,1,
                           1,1,0,0,0,1, 0,1,1,0,1,0, 0,0,1,1,0,0 };

    uchar marker_X[36] = { 1,0,0,0,0,1, 0,1,0,0,1,0, 0,1,1,1,0,0,
                           0,0,0,1,0,0, 0,1,0,1,1,0, 1,0,0,0,0,1 };

    cv::Mat mat_marker_O(6, 6, CV_8UC1, marker_O);
    cv::Mat mat_marker_X(6, 6, CV_8UC1, marker_X);

    m_dict->bytesList.push_back(cv::aruco::Dictionary::getByteListFromBits(mat_marker_O));
    m_dict->bytesList.push_back(cv::aruco::Dictionary::getByteListFromBits(mat_marker_X));

    m_params = new cv::aruco::DetectorParameters();
}

void Process::run()
{
    cv::Mat image_cv, image_cv_old;
    cv::Mat gray_cv, gray_cv_old;
    std::map<int, std::vector<cv::Point2f>> markerCorners_last;
    bool isStart = true;

    while(!m_isEnd) {
        PFrame frame = m_buffer_in->take();
        if(!frame.isValid) {
            thread()->msleep(40);
            continue;
        }

        image_cv = frame.image_cv;
        if(isStart) {
            image_cv_old = image_cv.clone();
            frame = m_buffer_in->take();
            if(!frame.isValid) {
                thread()->msleep(40);
                continue;
            }
            isStart = false;
        }

        cv::cvtColor(image_cv, gray_cv, CV_BGR2GRAY);
        cv::cvtColor(image_cv_old, gray_cv_old, CV_BGR2GRAY);

        // track
        std::vector<cv::Point2f> corners[2];
        std::vector<cv::Point2f> corners_O, corners_X;
        std::vector<uchar> status;
        std::vector<float> err;
        cv::Size subPixWinSize(7, 7), winSize(15, 15);
        cv::TermCriteria opts(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 30, 0.1);
        if(markerCorners_last.size() == 2) {
            // collect corners
            for(auto it : markerCorners_last) { corners[0].insert(corners[0].end(), it.second.begin(), it.second.end()); }

            // get subpixel accuracy
            cv::cornerSubPix(gray_cv, corners[0], subPixWinSize, cv::Size(-1, -1), opts);

            // track
            cv::calcOpticalFlowPyrLK(gray_cv_old, gray_cv, corners[0], corners[1], status, err, winSize, 2, opts);

            // save
            corners_O.insert(corners_O.begin(), corners[1].begin() + 0, corners[1].begin() + 4);
            corners_X.insert(corners_X.begin(), corners[1].begin() + 4, corners[1].begin() + 8);
        }

        // detect
        std::vector<int> markerIds;
        std::vector<std::vector<cv::Point2f>> markerCorners, rejectedCandidates;
        cv::aruco::detectMarkers(image_cv, m_dict, markerCorners, markerIds, m_params, rejectedCandidates);

        switch(markerIds.size()) {
        case 0: /* both not detected */
        {
            // update both with tracking
            if(markerCorners_last.size() == 2) {
                markerCorners_last[0] = corners_O;
                markerCorners_last[1] = corners_X;
            }
        }
            break;
        case 1: /* only one is detected */
        {
            // update undetected with tracking
            if(markerCorners_last.size() == 2) {
                if(std::find(markerIds.begin(), markerIds.end(), 0) != markerIds.end()) {
                    markerCorners_last[0] = markerCorners[0];
                    markerCorners_last[1] = corners_X;
                }
                else {
                    markerCorners_last[0] = corners_O;
                    markerCorners_last[1] = markerCorners[0];
                }
            }
        }
            break;
        case 2: /* both detected */
        {
            markerCorners_last[markerIds[0]] = markerCorners[0];
            markerCorners_last[markerIds[1]] = markerCorners[1];
        }
            break;
        default:
            break;
        }

        // push
        frame.corners_cv = markerCorners_last;
        if(frame.corners_cv.size() == 2) {
            frame.isCalculated = true;

            corners_O = frame.corners_cv[0];
            corners_X = frame.corners_cv[1];

            // find centroids
            cv::Moments mom_X = cv::moments(corners_X);
            cv::Point2f c_X(mom_X.m10 / mom_X.m00, mom_X.m01 / mom_X.m00);

            cv::Moments mom_O = cv::moments(corners_O);
            cv::Point2f c_O(mom_O.m10 / mom_O.m00, mom_O.m01 / mom_O.m00);

            // find closest point indexes
            double min = INT_MAX;
            int id = 0;
            int id_X = 0;
            for(auto & p : corners_X) {
                double d = pow(p.x - c_O.x, 2) + pow(p.y - c_O.y, 2);
                if(d < min) {
                    min = d;
                    id_X = id;
                }
                id++;
            }

            min = INT_MAX;
            id = 0;
            int id_O = 0;
            for(auto & p : corners_O) {
                double d = pow(p.x - c_X.x, 2) + pow(p.y - c_X.y, 2);
                if(d < min) {
                    min = d;
                    id_O = id;
                }
                id++;
            }

            // get the lower marker
            std::vector<cv::Point2f> srcPts, dstPts;
            std::vector<cv::Point2f> corners_H;
            int id_H;
            if(c_O.y > c_X.y) { corners_H = corners_O; id_H = id_O; frame.marker_id = 0; }
            else { corners_H = corners_X; id_H = id_X; frame.marker_id = 1; }

            qDebug() << id_H;

            // calculate hmg
            srcPts.push_back(corners_H[id_H]);
            dstPts.push_back(cv::Point2f(0, 0));

            int id_L = id_H - 1;
            int id_R = id_H + 1;
            int id_P = id_H + 2;
            if(id_L == -1) id_L = 3;
            if(id_R == 4) id_R = 0;
            if(id_P == 4) id_P = 0;
            if(id_P == 5) id_P = 1;

            srcPts.push_back(corners_H[id_L]);
            dstPts.push_back(cv::Point2f(-50, 0));

            srcPts.push_back(corners_H[id_R]);
            dstPts.push_back(cv::Point2f(0, -50));

            srcPts.push_back(corners_H[id_P]);
            dstPts.push_back(cv::Point2f(-50, -50));

            frame.homography_cv = cv::getPerspectiveTransform(dstPts, srcPts);
            frame.homography = QTransform(frame.homography_cv.at<double>(0, 0), frame.homography_cv.at<double>(1, 0), frame.homography_cv.at<double>(2, 0),
                                          frame.homography_cv.at<double>(0, 1), frame.homography_cv.at<double>(1, 1), frame.homography_cv.at<double>(2, 1),
                                          frame.homography_cv.at<double>(0, 2), frame.homography_cv.at<double>(1, 2), frame.homography_cv.at<double>(2, 2));
        }

        m_buffer_out->add(frame);

    }
}
